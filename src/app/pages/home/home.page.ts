import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HomeService} from "../../service/home.service";
import {ModalController} from '@ionic/angular';
import {DetalleComponent} from '../../component/detalle/detalle.component';
import {AddComponent} from '../../component/add/add.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  id: string;
  result: any;


  constructor(private route: ActivatedRoute, private homeService: HomeService, private router: Router,
              public modalCtrl: ModalController) {}

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.homeService.getBird(this.id)
          .subscribe(
              (data) => { // Success
                this.result = data;
                //console.log(data);
              },
              (error) => {
                console.error(error);
              }
          );
    });
  }

  getDetalleAve(id: number)
  {
    this.router.navigate(['/detalle', id, this.id]);
 
  }

  async detalleModal(id: number) {

    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: { id: id }
    });
    return await modal.present();

    
  }

  async addModal() {
    const modal = await this.modalCtrl.create({
      component: AddComponent,
      componentProps: { id: this.id },
    });
    
    return await modal.present();

    
  }

}

import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../../service/login.service';
import { LoadingController } from "@ionic/angular";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    user: string;
    password: string;
    private datos: any;
    private loading;

    constructor(private loginService: LoginService, private router: Router, private LoadCtrl: LoadingController) { }

    ngOnInit() {
    }
    login()
    {

        this.LoadCtrl.create({
            message: "Cargando"
        }).then((overlay) =>{
            this.loading = overlay;
            this.loading.present();
        })

        if(this.user.length > 0 && this.password.length > 0) {
            this.loginService.login(this.user, this.password).subscribe(res => {
                    this.loading.dismiss();

                    if (res["status"] == "OK")
                    {
                        //console.log(res["id"]);
                        this.router.navigate(['/home', res["id"]]);
                    }
                    else {
                        alert(res["message"]);
                    }
                },
                error => {
                    console.log(error);
                }
            );
        }
    }

}

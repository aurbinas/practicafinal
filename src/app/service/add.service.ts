import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddService {

  constructor(private httpClient: HttpClient) { }

  addBird(id: string, name, description)
  {
    const formData = new FormData();
    formData.append('idUser', id);
    formData.append('bird_name', name);
    formData.append('bird_description', description);

    return this.httpClient.post('http://dev.contanimacion.com/birds/public/addBird/',
        formData);
  }
}

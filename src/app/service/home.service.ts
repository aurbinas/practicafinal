import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpClient: HttpClient) { }

  getBird(iduser: string) {
    return this.httpClient.get('http://dev.contanimacion.com/birds/public/getBirds/' + iduser);
  }
}

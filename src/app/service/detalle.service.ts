import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DetalleService {

  constructor(private httpClient: HttpClient) { }

  getBirdDetalle(id: number) {
    return this.httpClient.get('http://dev.contanimacion.com/birds/public/getBirdDetails/'+id);
  }

  addBirdLocation(id: string, place, lng, lat)
  {
    console.log(id);
    const formData = new FormData();
    formData.append('idAve', id);
    formData.append('place', place);
    formData.append('long', lng);
    formData.append('lat', lat);


    return this.httpClient.post('http://dev.contanimacion.com/birds/public/addSighting/',
        formData);
  }
}

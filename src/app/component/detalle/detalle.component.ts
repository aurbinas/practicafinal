import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';
import {DetalleService} from "../../service/detalle.service";
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {

  constructor(private modalCtrl : ModalController, private  navParams : NavParams,
              private detalleService: DetalleService, public geolocation: Geolocation) { }
  idAve: number;
  result: any;
  avistamientos: any = [];
  closeResult: any;

  todo = {}

  place: string = "";
  long: number = 0;
  lat: number = 0;
  getLastId: number;

  ngOnInit() {
    this.idAve = this.navParams.get('id');
    this.getGeolocation();
    this.detalleService.getBirdDetalle(this.idAve)
        .subscribe(
            (data) => { // Success
              this.result = data;
              this.avistamientos = this.result[0].sightings_list;
              this.getLastId = this.avistamientos[this.avistamientos.length - 1];
            },
            (error) => {
              console.error(error);
            }
        );
  }

  closeChat()
  {
    this.modalCtrl.dismiss();
  }

  logForm() {
    console.log(this.todo);
  }

  addAve()
  {
    if (this.place.length > 0 && this.lat != 0 && this.long != 0)
    {
      console.log(this.place);
   this.detalleService.addBirdLocation(String(this.idAve), this.place, this.long, this.lat).subscribe(res =>
      {
        this.detalleService.getBirdDetalle(this.idAve)
        .subscribe(
            (data) => { // Success
              this.result = data;
              this.avistamientos = this.result[0].sightings_list;
              this.getLastId = this.avistamientos[this.avistamientos.length - 1];
            },
            (error) => {
              console.error(error);
            }
        );
      });

    }
    else
    {
      alert("Debe agregar todos los campos");
    }
  }


  getGeolocation(){    
    this.geolocation.getCurrentPosition().then((geoPosition: Geoposition) => {
      console.log(geoPosition);

      this.lat = geoPosition.coords.latitude;
      this.long = geoPosition.coords.longitude;

    })
  }
}

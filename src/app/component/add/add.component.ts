import { Component, OnInit } from '@angular/core';
import {AlertController, ModalController, NavParams} from '@ionic/angular';
import {AddService} from '../../service/add.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent implements OnInit {

  bird_name: string = "";
  bird_description: string = "";
  id: number;

  constructor(private modalCtrl : ModalController, private  navParams : NavParams,
              private alertCtrl: AlertController, private addService: AddService) { }

  ngOnInit() {
    this.id = this.navParams.get('id');
  }

  closeChat()
  {
    this.modalCtrl.dismiss();
  }

  addAve()
  {
    if (this.bird_name.length > 0 && this.bird_description.length > 0)
    {
      this.addService.addBird(String(this.id), this.bird_name, this.bird_description).subscribe(res =>
      {
        console.log(res);
        this.aveAlert();
      });
    }
    else
    {
      alert("Debe agregar todos los campos");
    }
  }

  async aveAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Información',
      message: 'Ave agregada.',
      buttons: ['OK']
    });

    await alert.present();
  }
}
